﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Hosting;
using System.Web.Http;

namespace ApiPayslipTaywin.Controllers
{
    public class InAppUpdateController : ApiController
    {
        [HttpGet]
        [Route("api/is_apks/latest")]
        public HttpResponseMessage apkLatest()
        {
            try
            {
                string fileName = "staff-pay-slip.apk";
                string localFilePath = HostingEnvironment.MapPath("/") + @"/is_apks/latest/" + fileName;

                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = fileName;
                //response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileName));

                return response;
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpGet]
        [Route("api/is_apks/version")]
        public IHttpActionResult getVersion()
        {
            try
            {
              
                string path = HostingEnvironment.MapPath("/") + @"/is_apks/";
                var list = new List<string>();

                foreach (string file in Directory.EnumerateFiles(path, "*", SearchOption.TopDirectoryOnly))
                {
                    list.Add(Path.GetFileNameWithoutExtension(file));
                }

                return Ok(list);
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }
    }
}
