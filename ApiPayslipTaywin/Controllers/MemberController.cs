﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ApiPayslipTaywin.Models;

namespace ApiPayslipTaywin.Controllers
{
    public class MemberController : ApiController
    {

        [HttpGet]
        [Route("api/member/{empCode}")]
        public IHttpActionResult getMemberTaywin(string empCode)
        {
            try
            {
                return Ok(DBTaywin.GetMemberTaywin(empCode));
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpGet]
        [Route("api/member_slip/{empCode}/{monPay}/{isSpecialPeriod}")]
        public IHttpActionResult getMemberSlipTaywin(string empCode, string monPay, string isSpecialPeriod)
        {
            try
            {
                return Ok(DBTaywin.GetMemberSlipTaywin(empCode, monPay, isSpecialPeriod));
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpGet]
        [Route("api/member_slip_debit/{empCode}/{isSpecialPeriod}/{year}/{monPay}")]
        public IHttpActionResult getDebitTaywin(string empCode, string isSpecialPeriod, string year, string monPay)
        {
            try
            {
                return Ok(DBTaywin.GetDebitTaywin(empCode, isSpecialPeriod, year, monPay));
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpGet]
        [Route("api/member_slip_credit/{empCode}/{isSpecialPeriod}/{year}/{monPay}")]
        public IHttpActionResult getCreditTaywin(string empCode, string isSpecialPeriod, string year, string monPay)
        {
            try
            {
                return Ok(DBTaywin.GetCreditTaywin(empCode, isSpecialPeriod, year, monPay));
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpGet]
        [Route("api/member_slip_total/{empCode}/{year}/{isSpecialPeriod}/{monPay}")]
        public IHttpActionResult getReportTotalSumYearTaywin(string empCode, string year, string isSpecialPeriod, string monPay)
        {
            try
            {
                return Ok(DBTaywin.GetReportTotalSumYearTaywin(empCode, year, isSpecialPeriod, monPay));
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpPost]
        [Route("api/member/check/data_from_taywin")]
        public IHttpActionResult IsCreateMember([FromBody] MemberModel member)
        {
            try
            {
                return Ok(DBTaywin.getTaywin(member.id13, member.fname, member.lname, member.birth_date));

            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpGet]
        [Route("api/member_test/{empCode}")]
        public IHttpActionResult getMemberTaywinTest(string empCode)
        {
            try
            {
                return Ok(DBTaywinTest.GetMemberTaywin(empCode));
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpGet]
        [Route("api/member_slip_test/{empCode}/{monPay}/{isSpecialPeriod}")]
        public IHttpActionResult getMemberSlipTaywinTest(string empCode, string monPay, string isSpecialPeriod)
        {
            try
            {
                return Ok(DBTaywinTest.GetMemberSlipTaywin(empCode, monPay, isSpecialPeriod));
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpGet]
        [Route("api/member_slip_debit_test/{empCode}/{isSpecialPeriod}/{year}/{monPay}")]
        public IHttpActionResult getDebitTaywinTest(string empCode, string isSpecialPeriod, string year, string monPay)
        {
            try
            {
                return Ok(DBTaywinTest.GetDebitTaywin(empCode, isSpecialPeriod, year, monPay));
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpGet]
        [Route("api/member_slip_credit_test/{empCode}/{isSpecialPeriod}/{year}/{monPay}")]
        public IHttpActionResult getCreditTaywinTest(string empCode, string isSpecialPeriod, string year, string monPay)
        {
            try
            {
                return Ok(DBTaywinTest.GetCreditTaywin(empCode, isSpecialPeriod, year, monPay));
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpGet]
        [Route("api/member_slip_total_test/{empCode}/{year}/{isSpecialPeriod}/{monPay}")]
        public IHttpActionResult getReportTotalSumYearTaywinTest(string empCode, string year, string isSpecialPeriod, string monPay)
        {
            try
            {
                return Ok(DBTaywinTest.GetReportTotalSumYearTaywin(empCode, year, isSpecialPeriod, monPay));
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpPost]
        [Route("api/member_test/check/data_from_taywin")]
        public IHttpActionResult IsCreateMemberTest([FromBody] MemberModel member)
        {
            try
            {
                return Ok(DBTaywinTest.getTaywin(member.id13, member.fname, member.lname, member.birth_date));

            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpPost]
        [Route("api/member/signup")]
        public IHttpActionResult createMember([FromBody] MemberModel member)
        {
            var fName = member.fname;
            var lName = member.lname;
            var id13 = member.id13;
            var birthDate = member.birth_date;
            var mPassword = member.password;

            try
            {
                if (DbMember.checkDataInToDatabaseMember(id13) )
                {
                    return Ok(false);
                }
                else 
                {
                    DbMember.CreateMember(fName, lName, id13, birthDate, mPassword);
                    return Ok(true);
                } 
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpPost]
        [Route("api/member/chengpassword")]
        public IHttpActionResult chengePasswordMember([FromBody] NewPasswordModel newPass) 
        {
            var id13 = newPass.id13;
            var oldPassword = newPass.oldPassword;
            var newPassword = newPass.newPassword;

            try
            {
                if(DbMember.CreateNewPassword(id13, oldPassword, newPassword))
                {
                    return Ok(true);
                }
                else
                {
                    return Ok(false);
                }
                
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }

        }

        [HttpPost]
        [Route("api/member/signin")]
        public IHttpActionResult loginMember([FromBody] MemberModel member)
        {
            try
            {
                if (DbMember.checkRegisterInToDatabase(member.id13, member.password))
                {

                    return Ok(DbMember.GetMember(member.id13, member.password));
                }
                else
                {
                    return Ok(DbMember.GetMember(member.id13, member.password));
                }
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpPost]
        [Route("api/member/check_signin")]
        public IHttpActionResult checkLoginMember([FromBody] MemberModel member)
        {
            try
            {
                var token = member.token;
                if (token.Contains("token="))
                {
                    var id13 = StringCode.Base64Decode(token.Replace("token=", ""));
                    return Ok(DbMember.SaveMemberByLogin(id13));
                }
                else
                {
                    return Ok(false);
                }
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpDelete]
        [Route("api/dellete/member/{citizenID}")]
        public IHttpActionResult DeleteEmploye(string citizenID)
        {
            try
            {
                if (DbMember.DeleteMember(citizenID))
                {
                   
                    return BadRequest("Record has not success Deleted");
                }
                else
                {
                    return Ok("Record has successfully Deleted");
                }

            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }
    }

}