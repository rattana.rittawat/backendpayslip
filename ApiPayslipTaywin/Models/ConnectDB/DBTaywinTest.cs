﻿using ApiPayslipTaywin.Models.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace ApiPayslipTaywin.Models
{
    public class DBTaywinTest
    {                                       
        private static string connetion18 = "Data Source=192.168.10.18;Initial Catalog=jojobraoil_2563;User ID=branchsliponline;Password=P@ssw0rdhr";

        //ประกาศ Cultureinfo ของแต่ละแบบที่ต้องการ
        public static CultureInfo thCulture = new CultureInfo("th-TH");
        public static CultureInfo ukCulture = new CultureInfo("UK");

        public static ResponseStatus getTaywin(string IdentityCard, string fName, string lName, string birthDate)
        {
            var strQuery = $"Select * FROM [jojobraoil_2560].[dbo].[UV_TWGTotalpaslipfrmlncDeducPyear]  where IdentityCard='{IdentityCard}' and FirstName='{fName}' and LastName like '%{lName}%' and BirthDate='{birthDate}' " +
                 $"Union all Select * FROM UV_TWGTotalpaslipfrmlncDeducPyear where IdentityCard='{IdentityCard}' and FirstName='{fName}' and LastName like '%{lName}%' and BirthDate='{birthDate}'";

            SqlConnection connectionSqlserver;
            SqlCommand command;

            connectionSqlserver = new SqlConnection(connetion18);

            connectionSqlserver.Open();
            command = new SqlCommand(strQuery, connectionSqlserver);

            var reader = command.ExecuteReader();
            var item = new ResponseStatus();
            item.errorMessage = false;
            if (reader.HasRows)
            {
                var okResponse = new HttpResponseMessage(HttpStatusCode.OK);

                {
                    item.statusCode = Convert.ToInt32(okResponse.StatusCode);
                    item.errorMessage = true;
                }
            }

            command.Dispose();
            connectionSqlserver.Close();

            return item;
        }

        public static string ListMonthArray(int pos)
        {
            //"ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ต.", "พ.ย.", "ธ.ค."
            string[] listMontNum = { "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค." };

            return listMontNum[pos - 1];
        }

        public static Employee GetMemberTaywin(string empCode)
        {
            var strQuery = $"Select * FROM [jojobraoil_2560].[dbo].[UV_TWGTotalpaslipfrmlncDeducPyear]  WHERE IdentityCard='{empCode}' " +
                $"Union all Select* FROM UV_TWGTotalpaslipfrmlncDeducPyear WHERE IdentityCard='{empCode}' order by PayDate desc";


            DateTime date1;

            SqlConnection connectionSqlserver;
            SqlCommand command;

            connectionSqlserver = new SqlConnection(connetion18);

            connectionSqlserver.Open();
            command = new SqlCommand(strQuery, connectionSqlserver);

            var reader = command.ExecuteReader();
            var item = new Employee();
            if (reader.HasRows)
            {
                var okResponse = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    ReasonPhrase = "Success"
                };

                {
                    item.statusCode = Convert.ToInt32(okResponse.StatusCode);
                    item.errorMessage = okResponse.ReasonPhrase;

                    item.data = new DataList();

                    var isF = true;

                    while (reader.Read())
                    {
                        if (isF)
                        {
                            item.data.EmplGrupName = Convert.ToString(reader["EmplGrupName"]);
                            item.data.OrgUnitName = Convert.ToString(reader["OrgUnitName"]);
                            item.data.OrgName = Convert.ToString(reader["OrgName"]);
                            item.data.OrgNameEng = Convert.ToString(reader["OrgNameEng"]);
                            item.data.Title = Convert.ToString(reader["Title"]);
                            item.data.FirstName = Convert.ToString(reader["FirstName"]);
                            item.data.LastName = Convert.ToString(reader["LastName"]);
                            item.data.EmpCode = Convert.ToString(reader["EmpCode"]);
                            item.data.WorkStartDate = (Convert.ToDateTime(reader["WorkStartDate"])).ToString("dd-MM-yyyy", thCulture);
                            item.data.IdentityCard = Convert.ToString(reader["IdentityCard"]);
                            item.data.BirthDate = Convert.ToString(reader["BirthDate"]);
                            item.data.PositionName = Convert.ToString(reader["PositionName"]);
                            item.data.BookNo = Convert.ToString(reader["BookNo"]);
                            item.data.BankName = Convert.ToString(reader["BankName"]);

                            item.data.month = new List<MonthList>();
                            item.data.month.Add(new MonthList
                            {
                                AllMonth = ListMonthArray(Convert.ToInt32(reader["monPay"])),
                                Name = Convert.ToString(reader["Name"]),
                                monPay = Convert.ToString(reader["monPay"]),
                                IsSpecialPeriod = Convert.ToString(reader["IsSpecialPeriod"]),
                                PeriodYear = Convert.ToInt32(reader["PeriodYear"]),
                                Periodno = Convert.ToInt32(reader["Periodno"]),
                                PayDate = (Convert.ToDateTime(reader["PayDate"])).ToString("dd-MM-yyyy", thCulture),
                                Salary = Convert.ToDecimal(reader["Salary"]),
                                NetIncome = Convert.ToDecimal(reader["NetIncome"]),
                                TotalIncome = Convert.ToDecimal(reader["TotalIncome"]),
                                TotalDeduct = Convert.ToDecimal(reader["TotalDeduct"]),
                                Social = Convert.ToDecimal(reader["Social"]),
                                ProvidentFund = Convert.ToDecimal(reader["ProvidentFund"]),
                                EmpPaytax = Convert.ToDecimal(reader["EmpPaytax"]),
                                OtherIncome = Convert.ToDecimal(reader["OtherIncome"]),
                                OtherDeduct = Convert.ToDecimal(reader["OtherDeduct"]),
                                EmplGrupCode = Convert.ToString(reader["EmplGrupCode"]),
                                OT = Convert.ToInt32(reader["OT"]),
                                Commission = Convert.ToInt32(reader["Commission"]),
                                incomother = Convert.ToInt32(reader["incomother"]),
                                deductother = Convert.ToInt32(reader["deductother"])
                            }
                            );
                        }
                        else
                        {
                            item.data.month.Add(new MonthList
                            {
                                AllMonth = ListMonthArray(Convert.ToInt32(reader["monPay"])),
                                Name = Convert.ToString(reader["Name"]),
                                monPay = Convert.ToString(reader["monPay"]),
                                IsSpecialPeriod = Convert.ToString(reader["IsSpecialPeriod"]),
                                PeriodYear = Convert.ToInt32(reader["PeriodYear"]),
                                Periodno = Convert.ToInt32(reader["Periodno"]),
                                PayDate = (Convert.ToDateTime(reader["PayDate"])).ToString("dd-MM-yyyy", thCulture),
                                Salary = Convert.ToDecimal(reader["Salary"]),
                                NetIncome = Convert.ToDecimal(reader["NetIncome"]),
                                TotalIncome = Convert.ToDecimal(reader["TotalIncome"]),
                                TotalDeduct = Convert.ToDecimal(reader["TotalDeduct"]),
                                Social = Convert.ToDecimal(reader["Social"]),
                                ProvidentFund = Convert.ToDecimal(reader["ProvidentFund"]),
                                EmpPaytax = Convert.ToDecimal(reader["EmpPaytax"]),
                                OtherIncome = Convert.ToDecimal(reader["OtherIncome"]),
                                OtherDeduct = Convert.ToDecimal(reader["OtherDeduct"]),
                                EmplGrupCode = Convert.ToString(reader["EmplGrupCode"]),
                                OT = Convert.ToInt32(reader["OT"]),
                                Commission = Convert.ToInt32(reader["Commission"]),
                                incomother = Convert.ToInt32(reader["incomother"]),
                                deductother = Convert.ToInt32(reader["deductother"])
                            });
                        }
                        isF = false;
                    }
                };
            }

            command.Dispose();
            connectionSqlserver.Close();

            return item;
        }

        public static Employee GetMemberSlipTaywin(string empCode, string monPay, string isSpecialPeriod) 
        {
            var strQuery = $"Select * FROM [jojobraoil_2560].[dbo].[UV_TWGTotalpaslipfrmlncDeducPyear]  " +
                $"WHERE EmpCode='{empCode}' and monPay='{monPay}' and IsSpecialPeriod='{isSpecialPeriod}' " +
                $"Union all Select* FROM UV_TWGTotalpaslipfrmlncDeducPyear " +
                $"WHERE EmpCode='{empCode}' and monPay='{monPay}' and IsSpecialPeriod='{isSpecialPeriod}' ";


            SqlConnection connectionSqlserver;
            SqlCommand command;

            connectionSqlserver = new SqlConnection(connetion18);

            connectionSqlserver.Open();
            command = new SqlCommand(strQuery, connectionSqlserver);

            var reader = command.ExecuteReader();
            var item = new Employee();

            if (reader.HasRows)
            {
                var okResponse = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    ReasonPhrase = "Success"
                };

                {
                    item.statusCode = Convert.ToInt32(okResponse.StatusCode);
                    item.errorMessage = okResponse.ReasonPhrase;

                    item.data = new DataList();

                    var isF = true;

                    while (reader.Read())
                    {
                        if (isF)
                        {
                            item.data.EmplGrupName = Convert.ToString(reader["EmplGrupName"]);
                            item.data.OrgUnitName = Convert.ToString(reader["OrgUnitName"]);
                            item.data.OrgName = Convert.ToString(reader["OrgName"]);
                            item.data.OrgNameEng = Convert.ToString(reader["OrgNameEng"]);
                            item.data.Title = Convert.ToString(reader["Title"]);
                            item.data.FirstName = Convert.ToString(reader["FirstName"]);
                            item.data.LastName = Convert.ToString(reader["LastName"]);
                            item.data.EmpCode = Convert.ToString(reader["EmpCode"]);
                            item.data.WorkStartDate = (Convert.ToDateTime(reader["WorkStartDate"])).ToString("dd-MM-yyyy", thCulture);
                            item.data.IdentityCard = Convert.ToString(reader["IdentityCard"]);
                            item.data.BirthDate = Convert.ToString(reader["BirthDate"]);
                            item.data.PositionName = Convert.ToString(reader["PositionName"]);
                            item.data.BookNo = Convert.ToString(reader["BookNo"]);
                            item.data.BankName = Convert.ToString(reader["BankName"]);

                            item.data.month = new List<MonthList>();
                            item.data.month.Add(new MonthList
                            {
                                AllMonth = ListMonthArray(Convert.ToInt32(reader["monPay"])),
                                Name = Convert.ToString(reader["Name"]),
                                monPay = Convert.ToString(reader["monPay"]),
                                IsSpecialPeriod = Convert.ToString(reader["IsSpecialPeriod"]),
                                PeriodYear = Convert.ToInt32(reader["PeriodYear"]),
                                PayDate = (Convert.ToDateTime(reader["PayDate"])).ToString("dd-MM-yyyy", thCulture),
                                Salary = Convert.ToDecimal(reader["Salary"]),
                                NetIncome = Convert.ToDecimal(reader["NetIncome"]),
                                TotalIncome = Convert.ToDecimal(reader["TotalIncome"]),
                                TotalDeduct = Convert.ToDecimal(reader["TotalDeduct"]),
                                Social = Convert.ToDecimal(reader["Social"]),
                                ProvidentFund = Convert.ToDecimal(reader["ProvidentFund"]),
                                EmpPaytax = Convert.ToDecimal(reader["EmpPaytax"]),
                                OtherIncome = Convert.ToDecimal(reader["OtherIncome"]),
                                OtherDeduct = Convert.ToDecimal(reader["OtherDeduct"]),
                                EmplGrupCode = Convert.ToString(reader["EmplGrupCode"]),
                                OT = Convert.ToInt32(reader["OT"]),
                                Commission = Convert.ToInt32(reader["Commission"]),
                                incomother = Convert.ToInt32(reader["incomother"]),
                                deductother = Convert.ToInt32(reader["deductother"])
                            }
                            );
                        }
                        else
                        {
                            item.data.month.Add(new MonthList
                            {
                                AllMonth = ListMonthArray(Convert.ToInt32(reader["monPay"])),
                                Name = Convert.ToString(reader["Name"]),
                                monPay = Convert.ToString(reader["monPay"]),
                                IsSpecialPeriod = Convert.ToString(reader["IsSpecialPeriod"]),
                                PeriodYear = Convert.ToInt32(reader["PeriodYear"]),
                                PayDate = (Convert.ToDateTime(reader["PayDate"])).ToString("dd-MM-yyyy", thCulture),
                                Salary = Convert.ToDecimal(reader["Salary"]),
                                NetIncome = Convert.ToDecimal(reader["NetIncome"]),
                                TotalIncome = Convert.ToDecimal(reader["TotalIncome"]),
                                TotalDeduct = Convert.ToDecimal(reader["TotalDeduct"]),
                                Social = Convert.ToDecimal(reader["Social"]),
                                ProvidentFund = Convert.ToDecimal(reader["ProvidentFund"]),
                                EmpPaytax = Convert.ToDecimal(reader["EmpPaytax"]),
                                OtherIncome = Convert.ToDecimal(reader["OtherIncome"]),
                                OtherDeduct = Convert.ToDecimal(reader["OtherDeduct"]),
                                EmplGrupCode = Convert.ToString(reader["EmplGrupCode"]),
                                OT = Convert.ToInt32(reader["OT"]),
                                Commission = Convert.ToInt32(reader["Commission"]),
                                incomother = Convert.ToInt32(reader["incomother"]),
                                deductother = Convert.ToInt32(reader["deductother"])
                            });
                        }
                        isF = false;
                    }
                };
            }

            command.Dispose();
            connectionSqlserver.Close();

            return item;
        }

        public static DebitCreditTaywin GetCreditTaywin(string empCode, string isSpecialPeriod, string year, string monPay)
        {
            var strQuery = $"SELECT A.EmpCode, A.Title, A.FirstName, A.LastName, B.PayDate, B.AccountCode ,B.AccountName, B.IsSpecialPeriod, B.Credit " +
                $"FROM [jojobraoil_2560].[dbo].[UV_TWGTotalpaslipfrmlncDeducPyear] A lEFT JOIN [jojobraoil_2560].[dbo].[UV_DebitCreditTW] B " +
                $"ON A.EmpCode = B.EmpCode AND A.PeriodYear = B.PeriodYear AND A.Periodno = B.Periodno " +
                $"where A.EmpCode='{empCode}' and B.IsSpecialPeriod='{isSpecialPeriod}' and B.PeriodYear='{year}' and B.monPay='{monPay}' and B.Debit = 0 " +
                $"GROUP BY A.EmpCode, A.Title, A.FirstName, A.LastName, B.PayDate, B.AccountCode ,B.AccountName, B.IsSpecialPeriod, B.Credit " +
                $"union all SELECT A.EmpCode, A.Title, A.FirstName, A.LastName, B.PayDate, B.AccountCode ,B.AccountName, B.IsSpecialPeriod, B.Credit " +
                $"FROM [jojobraoil_2563].[dbo].[UV_TWGTotalpaslipfrmlncDeducPyear] A lEFT JOIN [jojobraoil_2563].[dbo].[UV_DebitCreditTW] B " +
                $"ON A.EmpCode = B.EmpCode AND A.PeriodYear = B.PeriodYear AND A.Periodno = B.Periodno " +
                $"where A.EmpCode='{empCode}' and B.IsSpecialPeriod='{isSpecialPeriod}' and B.PeriodYear='{year}' and B.monPay='{monPay}' and B.Debit = 0 " +
                $"GROUP BY A.EmpCode, A.Title, A.FirstName, A.LastName, B.PayDate, B.AccountCode ,B.AccountName, B.IsSpecialPeriod, B.Credit ";
            SqlConnection connectionSqlserver;
            SqlCommand command;

            connectionSqlserver = new SqlConnection(connetion18);

            connectionSqlserver.Open();
            command = new SqlCommand(strQuery, connectionSqlserver);

            var reader = command.ExecuteReader();
            var item = new DebitCreditTaywin();

            if (reader.HasRows)
            {
                var okResponse = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    ReasonPhrase = "Success"
                };

                {
                    item.statusCode = Convert.ToInt32(okResponse.StatusCode);
                    item.errorMessage = okResponse.ReasonPhrase;

                    item.data = new DebitCreditDataList();
                    var isF = true;

                    while (reader.Read())
                    {
                        if (isF)
                        {
                            item.data.EmpCode = Convert.ToString(reader["EmpCode"]);
                            item.data.PayDate = (Convert.ToDateTime(reader["PayDate"])).ToString("dd-MM-yyyy", thCulture);


                            item.data.accountData = new List<DataAccountList>();
                            item.data.accountData.Add(new DataAccountList
                            {
                                AccountCode = Convert.ToString(reader["AccountCode"]),
                                AccountName = Convert.ToString(reader["AccountName"]),
                                Credit = Convert.ToDecimal(reader["Credit"])

                            });
                        }
                        else
                        {
                            item.data.accountData.Add(new DataAccountList
                            {
                                AccountCode = Convert.ToString(reader["AccountCode"]),
                                AccountName = Convert.ToString(reader["AccountName"]),
                                Credit = Convert.ToDecimal(reader["Credit"])

                            });
                        }

                        isF = false;
                    };

                }
            }
            command.Dispose();
            connectionSqlserver.Close();
            return item;
        }

        public static DebitCreditTaywin GetDebitTaywin(string empCode, string isSpecialPeriod, string year, string monPay)
        {
            var strQuery = $"SELECT A.EmpCode, A.Title, A.FirstName, A.LastName, B.PayDate, B.AccountCode ,B.AccountName, B.IsSpecialPeriod, B.Debit " +
                $"FROM [jojobraoil_2560].[dbo].[UV_TWGTotalpaslipfrmlncDeducPyear] A lEFT JOIN [jojobraoil_2560].[dbo].[UV_DebitCreditTW] B " +
                $"ON A.EmpCode = B.EmpCode AND A.PeriodYear = B.PeriodYear AND A.Periodno = B.Periodno " +
                $"where A.EmpCode='{empCode}' and B.IsSpecialPeriod='{isSpecialPeriod}' and B.PeriodYear='{year}' and B.monPay='{monPay}' and B.Credit = 0 " +
                $"GROUP BY A.EmpCode, A.Title, A.FirstName, A.LastName, B.PayDate, B.AccountCode ,B.AccountName, B.IsSpecialPeriod, B.Debit " +
                $"union all SELECT A.EmpCode, A.Title, A.FirstName, A.LastName, B.PayDate, B.AccountCode ,B.AccountName, B.IsSpecialPeriod, B.Debit " +
                $"FROM [jojobraoil_2563].[dbo].[UV_TWGTotalpaslipfrmlncDeducPyear] A lEFT JOIN [jojobraoil_2563].[dbo].[UV_DebitCreditTW] B " +
                $"ON A.EmpCode = B.EmpCode AND A.PeriodYear = B.PeriodYear AND A.Periodno = B.Periodno " +
                $"where A.EmpCode='{empCode}' and B.IsSpecialPeriod='{isSpecialPeriod}' and B.PeriodYear='{year}' and B.monPay='{monPay}' and B.Credit = 0 " +
                $"GROUP BY A.EmpCode, A.Title, A.FirstName, A.LastName, B.PayDate, B.AccountCode ,B.AccountName, B.IsSpecialPeriod, B.Debit ";

            SqlConnection connectionSqlserver;
            SqlCommand command;

            connectionSqlserver = new SqlConnection(connetion18);

            connectionSqlserver.Open();
            command = new SqlCommand(strQuery, connectionSqlserver);

            var reader = command.ExecuteReader();
            var item = new DebitCreditTaywin();

            if (reader.HasRows)
            {
                var okResponse = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    ReasonPhrase = "Success"
                };

                {
                    item.statusCode = Convert.ToInt32(okResponse.StatusCode);
                    item.errorMessage = okResponse.ReasonPhrase;

                    item.data = new DebitCreditDataList();
                    var isF = true;

                    while (reader.Read())
                    {
                        if (isF)
                        {
                            item.data.EmpCode = Convert.ToString(reader["EmpCode"]);
                            item.data.PayDate = (Convert.ToDateTime(reader["PayDate"])).ToString("dd-MM-yyyy", thCulture);


                            item.data.accountData = new List<DataAccountList>();
                            item.data.accountData.Add(new DataAccountList
                            {
                                AccountCode = Convert.ToString(reader["AccountCode"]),
                                AccountName = Convert.ToString(reader["AccountName"]),
                                Debit = Convert.ToDecimal(reader["Debit"])

                            });
                        }
                        else
                        {
                            item.data.accountData.Add(new DataAccountList
                            {
                                AccountCode = Convert.ToString(reader["AccountCode"]),
                                AccountName = Convert.ToString(reader["AccountName"]),
                                Debit = Convert.ToDecimal(reader["Debit"])

                            });
                        }

                        isF = false;
                    };

                }
            }
            command.Dispose();
            connectionSqlserver.Close();
            return item;
        }

        public static StatusResponseTaywin GetReportTotalSumYearTaywin(string empCode, string year, string isSpecialPeriod, string monPay) 
        {
            var strQuery = $"SELECT EmpCode, PeriodYear, Sum([Salary]) as SumSalary ,SUM([NetIncome]) AS SumNetIncome, " +
                $"SUM([TotalIncome]) as SumTotalIncome ,Sum([TotalDeduct]) as SumTotalDeduct ,SUM([Social]) as SumSocial, " +
                $"SUM([ProvidentFund]) as SumProvidentFund, Sum([EmpPaytax]) as SumEmpPaytax, sum([OtherIncome]) as SumOtherIncome, " +
                $"sum([OtherDeduct]) as SumOtherDeduct, SUM([ProvidentFundEmployer]) as SumProvidentEmployer, SUM([ProvidentFund]+[ProvidentFundEmployer]) as SumProvident " +
                $"FROM [jojobraoil_2563].[dbo].[UV_TWGTotalpaslipfrmlncDeducPyear] " +
                $"where EmpCode='{empCode}' and PeriodYear='{year}' and IsSpecialPeriod='{isSpecialPeriod}' and monPay <= Convert(int,'{monPay}') Group By[EmpCode],[PeriodYear],[IsSpecialPeriod] " +
                $"UNION ALL " +
                $"SELECT EmpCode, PeriodYear, Sum([Salary]) as SumSalary ,SUM([NetIncome]) AS SumNetIncome, " +
                $"SUM([TotalIncome]) as SumTotalIncome ,Sum([TotalDeduct]) as SumTotalDeduct ,SUM([Social]) as SumSocial," +
                $"SUM([ProvidentFund]) as SumProvidentFund, Sum([EmpPaytax]) as SumEmpPaytax, sum([OtherIncome]) as SumOtherIncome, " +
                $"sum([OtherDeduct]) as SumOtherDeduct, SUM([ProvidentFundEmployer]) as SumProvidentEmployer, SUM([ProvidentFund]+[ProvidentFundEmployer]) as SumProvident " +
                $"FROM [jojobraoil_2560].[dbo].[UV_TWGTotalpaslipfrmlncDeducPyear] " +
                $"where EmpCode='{empCode}' and PeriodYear='{year}' and IsSpecialPeriod='{isSpecialPeriod}' and monPay <= Convert(int,'{monPay}') Group By[EmpCode],[PeriodYear],[IsSpecialPeriod] ";

            SqlConnection connectionSqlserver;
            SqlCommand command;

            connectionSqlserver = new SqlConnection(connetion18);

            connectionSqlserver.Open();
            command = new SqlCommand(strQuery, connectionSqlserver);

            var reader = command.ExecuteReader();
            var item = new StatusResponseTaywin();

            if (reader.HasRows)
            {
                var okResponse = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    ReasonPhrase = "Success"
                };

                {
                    item.statusCode = Convert.ToInt32(okResponse.StatusCode);
                    item.errorMessage = okResponse.ReasonPhrase;

                    item.dataTotalSum = new ReportTotalSumYear();
                    var isF = true;

                    while (reader.Read())
                    {
                        if (isF)
                        {
                            item.dataTotalSum.EmpCode = Convert.ToString(reader["EmpCode"]);
                            item.dataTotalSum.PeriodYear = Convert.ToInt32(reader["PeriodYear"]);
                            item.dataTotalSum.SumSalary = Convert.ToDecimal(reader["SumSalary"]);
                            item.dataTotalSum.SumNetIncome = Convert.ToDecimal(reader["SumNetIncome"]);
                            item.dataTotalSum.SumTotalIncome = Convert.ToDecimal(reader["SumTotalIncome"]);
                            item.dataTotalSum.SumTotalDeduct = Convert.ToDecimal(reader["SumTotalDeduct"]);
                            item.dataTotalSum.SumSocial = Convert.ToDecimal(reader["SumSocial"]);
                            item.dataTotalSum.SumProvidentFund = Convert.ToDecimal(reader["SumProvidentFund"]);
                            item.dataTotalSum.SumEmpPaytax = Convert.ToDecimal(reader["SumEmpPaytax"]);
                            item.dataTotalSum.SumOtherIncome = Convert.ToDecimal(reader["SumOtherIncome"]);
                            item.dataTotalSum.SumOtherDeduct = Convert.ToDecimal(reader["SumOtherDeduct"]);
                            item.dataTotalSum.SumProvidentEmployer = Convert.ToDecimal(reader["SumProvidentEmployer"]);
                            item.dataTotalSum.SumProvident = Convert.ToDecimal(reader["SumProvident"]);
                        }

                        isF = false;
                    };

                }
            }
            command.Dispose();
            connectionSqlserver.Close();
            return item;
        }

    }
}