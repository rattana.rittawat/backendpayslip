﻿using ApiPayslipTaywin.Models.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Text;

namespace ApiPayslipTaywin.Models
{
    public class DbMember
    {
        private static string connetionMe = "Data Source=.;Initial Catalog=pay_slip;Integrated Security=True";

        //ประกาศ Cultureinfo ของแต่ละแบบที่ต้องการ
        public static CultureInfo thCulture = new CultureInfo("th-TH");
        public static Calendar thaiCalendar = CultureInfo.CurrentCulture.Calendar;

        public static ResponseStatusLogin GetMember(string id13, string password)
        {
            SqlConnection connection;
            SqlCommand command;

            connection = new SqlConnection(connetionMe);

            connection.Open();
            command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "GetMember";
            command.Parameters.Add(new SqlParameter("@id13", id13));
            command.Parameters.Add(new SqlParameter("@password", password));
            command.Parameters.Add(new SqlParameter("@timeStamp", ""));

            var reader = command.ExecuteReader();
            var item = new ResponseStatusLogin();
            item.errorMessage = false;
            if (reader.HasRows)
            {
                var okResponse = new HttpResponseMessage(HttpStatusCode.OK);

                {
                    item.statusCode = Convert.ToInt32(okResponse.StatusCode);
                    item.errorMessage = true;
                   item.dataMember = new MemberModel();
                   while (reader.Read())
                   {
                       if (item.errorMessage)
                       {
                            item.dataMember.fname = Convert.ToString(reader["fname"]);
                            item.dataMember.lname = Convert.ToString(reader["lname"]);
                            item.dataMember.id13 = Convert.ToString(reader["id13"]);
                            item.dataMember.birth_date = Convert.ToString(reader["birth_date"]);
                            item.dataMember.password = Convert.ToString(reader["password"]);
                            item.dataMember.timeStamp = (Convert.ToDateTime(reader["timeStamp"])).ToString("dd/MM/yyyy HH:mm:ss", thCulture);
                            item.dataMember.token = "token=" + StringCode.Base64Encode(id13);
                        }
                   }
                }
            }

            command.Dispose();
            connection.Close();

            return item;
        }

        public static bool checkLoginInToDatabase(string idCard)
        {
            var f = false;

            var strQuery = $"select * from members where id13='{idCard}' ";
            SqlConnection connection;
            SqlCommand command;

            connection = new SqlConnection(connetionMe);

            connection.Open();
            command = new SqlCommand(strQuery, connection);

            var reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                f = true;
            }

            command.Dispose();
            connection.Close();

            return f;
        }

        public static MemberModel SaveMemberByLogin(string id13)
        {
            var member = new MemberModel();
            SqlConnection connection;
            SqlCommand command;

            connection = new SqlConnection(connetionMe);

            connection.Open();
            command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "SaveMemberByLogin";
            command.Parameters.Add(new SqlParameter("@id13", id13));

            var reader = command.ExecuteReader();
            if (reader.Read())
            {
                member.id = reader.GetInt32(0);
                member.fname = reader.GetString(1);
                member.lname = reader.GetString(2);
                member.id13 = reader.GetString(3);
                member.birth_date = Convert.ToString(reader.GetValue(4));
                member.password = reader.GetString(5);
                member.token = "token=" + StringCode.Base64Encode(id13);
            }

            command.Dispose();
            connection.Close();

            return member;
        }

        public static bool checkDataInToDatabaseMember(string idCard)
        {
            var strQuery = $"select * from members where id13='{idCard}'";
            SqlConnection connection;
            SqlCommand command;

            connection = new SqlConnection(connetionMe);

            connection.Open();
            command = new SqlCommand(strQuery, connection);

            var reader = command.ExecuteReader();
            var item = new ResponseStatus();
            item.errorMessage = false;
            if (reader.HasRows)
            {
                var okResponse = new HttpResponseMessage(HttpStatusCode.OK);

                {
                    item.statusCode = Convert.ToInt32(okResponse.StatusCode);
                    item.errorMessage = true;
                }
            }

            command.Dispose();
            connection.Close();

            return item.errorMessage;
        }

        public static bool checkRegisterInToDatabase(string idCard, string password)
        {
            var f = false;

            var strQuery = $"select * from members where id13='{idCard}' and password='{password}'";
            SqlConnection connection;
            SqlCommand command;

            connection = new SqlConnection(connetionMe);

            connection.Open();
            command = new SqlCommand(strQuery, connection);

            var reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                f = true;
            }

            command.Dispose();
            connection.Close();

            return f;
        }

        public static bool checkPasswordInToDatabaseMember(string idCard, string password)
        {
            var f = false;
        
           var strQuery = $"select * from members where id13='{idCard}' and password='{password}'";
            SqlConnection connection;
            SqlCommand command;
        
            connection = new SqlConnection(connetionMe);
        
            connection.Open();
            command = new SqlCommand(strQuery, connection);

            var reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                f = true;
            }
        
            command.Dispose();
            connection.Close();
        
            return f;
        }

        public static bool CreateNewPassword(string id13, string oldPassword, string newPassword)
        {

            SqlConnection connection;
            SqlCommand command;

            connection = new SqlConnection(connetionMe);

            connection.Open();
            command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "CreateNewPassword";
            
            command.Parameters.Add(new SqlParameter("@UserName", id13));
            command.Parameters.Add(new SqlParameter("@OldPassword", oldPassword));
            command.Parameters.Add(new SqlParameter("@NewPassword", newPassword));

            var ds = new DataSet();
            var adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            
            var dt = ds.Tables[0];
            var f = false;
            if(dt.Rows.Count > 0)
            {
                f = Convert.ToInt32(dt.Rows[0]["isCheck"]) == 1;
            }

            command.ExecuteReader();
            command.Dispose();
            connection.Close();

            return f;
        }

        public static void CreateMember(string fName, string lName, string citizenId, string birthDate, string mPassword)
        {
            SqlConnection connection;
            SqlCommand command;

            connection = new SqlConnection(connetionMe);

            connection.Open();
            command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "CreateMember";
            command.Parameters.Add(new SqlParameter("@fName", fName));
            command.Parameters.Add(new SqlParameter("@lName", lName));
            command.Parameters.Add(new SqlParameter("@id13", citizenId));
            command.Parameters.Add(new SqlParameter("@birthDate", birthDate));
            command.Parameters.Add(new SqlParameter("@password", mPassword));

            command.ExecuteNonQuery();

            command.Dispose();
            connection.Close();
        }

        public static void CreateMemberTest(string fName, string lName, string citizenId, string birthDate, string mPassword)
        {
            var txtPassword = Encoding.ASCII.GetBytes(mPassword);
            SqlConnection connection;
            SqlCommand command;

            connection = new SqlConnection(connetionMe);

            connection.Open();
            command = new SqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "CreateMember";
            command.Parameters.Add(new SqlParameter("@fName", fName));
            command.Parameters.Add(new SqlParameter("@lName", lName));
            command.Parameters.Add(new SqlParameter("@id13", citizenId));
            command.Parameters.Add(new SqlParameter("@birthDate", birthDate));
            command.Parameters.Add(new SqlParameter("@password", StringCode.Base64Encode(mPassword)));

            command.ExecuteNonQuery();

            command.Dispose();
            connection.Close();
        }

        public static bool DeleteMember(string citizenID)
        {
            //DELETE FROM Customers WHERE CustomerName='Alfreds Futterkiste';
            var strDelete = $"DELETE FROM members WHERE id13='{citizenID}'";
            SqlConnection connection;
            SqlCommand command;

            connection = new SqlConnection(connetionMe);

            connection.Open();
            command = new SqlCommand(strDelete, connection);

            var f = false;
            var reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                f = true;
            }

            command.Dispose();
            connection.Close();

            return f;
        }
    }
}