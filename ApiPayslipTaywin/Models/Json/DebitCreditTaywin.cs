﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiPayslipTaywin.Models.Json
{
    public class DebitCreditTaywin
    {

        public int statusCode { get; set; }
        public string errorMessage { get; set; }
        public DebitCreditDataList data { get; set; }

    }

    public class DebitCreditDataList
    {
        public string EmpCode { get; set; }
        public string PayDate { get; set; }
        public List<DataAccountList> accountData { get; set; }
    }

    public class DataAccountList
    {
        public string AccountCode { get; set; }
        public string AccountName { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
    }


}