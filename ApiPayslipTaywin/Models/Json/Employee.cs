﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiPayslipTaywin.Models.Json
{
    public class Employee
    {
        public int statusCode { get; set; }
        public string errorMessage { get; set; }
        public DataList data { get; set; }
    }

    public class DataList
    {
        public string EmplGrupName { get; set; }
        public string OrgUnitName { get; set; }
        public string OrgName { get; set; }
        public string OrgNameEng { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmpCode { get; set; }
        public string WorkStartDate { get; set; }
        public List<MonthList> month { get; set; }
        public string IdentityCard { get; set; }
        public string BirthDate { get; set; }
        public string PositionName { get; set; }
        public string BookNo { get; set; }
        public string BankName { get; set; }

    }

    public class MonthList
    {
        public string AllMonth { get; set; }
        public string Name { get; set; }
        public string monPay {get; set;}
        public string IsSpecialPeriod { get; set; }
        public int PeriodYear { get; set; }
        public int Periodno { get; set; }
        public string PayDate { get; set; }
        public decimal Salary { get; set; }
        public decimal NetIncome { get; set; }
        public decimal TotalIncome { get; set; }
        public decimal TotalDeduct { get; set; }
        public decimal Social { get; set; }
        public decimal ProvidentFund { get; set; }
        public decimal EmpPaytax { get; set; }
        public decimal OtherIncome { get; set; }
        public decimal OtherDeduct { get; set; }
        public string EmplGrupCode { get; set; }
        public int OT { get; set; }
        public int Commission { get; set; }
        public int incomother { get; set; }
        public int deductother { get; set; }
    }

}