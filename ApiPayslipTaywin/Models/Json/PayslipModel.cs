﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiPayslipTaywin.Models.Json
{
    public class PayslipModel
    { 
        public int statusCode { get; set; }
        public string errorMessage { get; set; }
        public AllDataList data { get; set; }
    }

    public class AllDataList
    {
        public string OrgName { get; set; }
        public string OrgNameEng { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmpCode { get; set; }
        public List<AllMonthList> month { get; set; }
        public string IdentityCard { get; set; }
        public string BirthDate { get; set; }

       /// public List<ExpensesList> expenses { get; set; }


    }

    public class AllMonthList
    {
        public string AllMonth { get; set; }
        public string Name { get; set; }
        public int PeriodYear { get; set; }
        public byte Periodno { get; set; }
        public string PayDate { get; set; }
        public decimal Salary { get; set; }
        public decimal NetIncome { get; set; }
        public decimal TotalIncome { get; set; }
        public decimal TotalDeduct { get; set; }
        public decimal Social { get; set; }
        public decimal ProvidentFund { get; set; }
        public decimal EmpPaytax { get; set; }
        public decimal OtherIncome { get; set; }
        public decimal OtherDeduct { get; set; }
        public string EmplGrupCode { get; set; }
        public int BranchNo { get; set; }
        public int OT { get; set; }
        public int Commission { get; set; }
        public int incomother { get; set; }
        public int deductother { get; set; }
    }

    //public class ExpensesList
    //{
    //    public string AccountName { get; set; }
    //    public decimal Debit { get; set; }
    //    public decimal Credit { get; set; }
    //}

}