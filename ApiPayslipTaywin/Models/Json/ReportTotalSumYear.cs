﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiPayslipTaywin.Models.Json
{
    public class ReportTotalSumYear
    {
      public string EmpCode { get; set; }
      public int PeriodYear { get; set; }
      public decimal SumSalary { get; set; }
      public decimal SumNetIncome { get; set; }
      public decimal SumTotalIncome { get; set; }
      public decimal SumTotalDeduct { get; set; }
      public decimal SumSocial { get; set; }
      public decimal SumProvidentFund { get; set; }
      public decimal SumEmpPaytax { get; set; }
      public decimal SumOtherIncome { get; set; }
      public decimal SumOtherDeduct { get; set; }
      public decimal SumProvidentEmployer { get; set; }
      public decimal SumProvident { get; set; }
    }
}