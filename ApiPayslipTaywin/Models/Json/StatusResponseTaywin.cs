﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiPayslipTaywin.Models.Json
{
    public class StatusResponseTaywin
    {
        public int statusCode { get; set; }
        public string errorMessage { get; set; }
        public ReportTotalSumYear dataTotalSum { get; set; } 
    }
}