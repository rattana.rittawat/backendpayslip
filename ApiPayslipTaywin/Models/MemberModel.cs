﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiPayslipTaywin.Models
{
    public class MemberModel
    {
        public int id { get; set; }

        public string fname { get; set; }

        public string lname { get; set; }

        public string id13 { get; set; }

        //public string staff_id { get; set; }

        public string birth_date { get; set; }

        public string password { get; set; }

        public string timeStamp { get; set; }

        public string token { get; set; }

        public MemberModel() { }
    }
}