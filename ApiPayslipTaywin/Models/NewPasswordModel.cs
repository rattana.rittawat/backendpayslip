﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiPayslipTaywin.Models
{
    public class NewPasswordModel
    {
        public string id13 { get; set; }
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
    }
}