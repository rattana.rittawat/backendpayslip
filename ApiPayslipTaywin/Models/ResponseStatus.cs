﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiPayslipTaywin.Models
{
    public class ResponseStatus
    {
        public int statusCode { get; set; }
        public bool errorMessage { get; set; }
    }
}