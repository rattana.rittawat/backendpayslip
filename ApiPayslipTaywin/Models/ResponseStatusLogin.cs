﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiPayslipTaywin.Models
{
    public class ResponseStatusLogin
    {
        public int statusCode { get; set; }
        public bool errorMessage { get; set; }

        public MemberModel dataMember { get; set; }
    }
}